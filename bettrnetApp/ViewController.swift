//
//  ViewController.swift
//  bettrnetApp
//
//  Created by David Leonard on 10/13/15.
//  Copyright © 2015 David Leonard. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let kKeychainItemName : String = "Bettrnet-Dashboard"
    let kClientID : String = "169309480683-ui5raatsseaj3ha0vq00cv1q7iic4nk5.apps.googleusercontent.com"
    let kClientSecret : String = "frKg1BUX357eGwDkv3NEbWee"
    let scope : String = "https://www.googleapis.com/auth/userinfo.email"
    
    var user:GTLUsersNinjaOwnerEntity! = nil
    var device:GTLDevicesDeviceEntityCollection! = nil
    var newDevice:GTLDevicesDeviceEntity? = nil
    var accountID:Int64! = nil
    var auth:GTMOAuth2Authentication! = nil
    
    @IBOutlet weak var deviceNameInput: UITextField!
    @IBOutlet weak var skipToStats: UIButton!
    @IBOutlet weak var wifiNetworkName: UITextField!
    @IBOutlet weak var createDeviceButton: UIButton!
    
    @IBAction func openSafariLink(sender: UIButton) {
        
        if let device = self.newDevice {
            let mobileConfigUrl = device.config.mobileConfigUrl.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
            
            let installMobileConfigUrl = NSURL(string: "http://www.abettr.net?mobileConfig=" + mobileConfigUrl)!
            
            print(installMobileConfigUrl)
            
            UIApplication.sharedApplication().openURL(installMobileConfigUrl)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showWaitIndicator(title:String) -> UIAlertView {
        //        println("showWaitIndicator \(title)")
        let progressAlert = UIAlertView()
        progressAlert.title = title
        progressAlert.message = "Please Wait...."
        progressAlert.show()
        
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityView.center = CGPointMake(progressAlert.bounds.size.width / 2, progressAlert.bounds.size.height - 45)
        progressAlert.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.startAnimating()
        return progressAlert
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isAuthed()
        {
            
            auth = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kKeychainItemName, clientID: kClientID, clientSecret: kClientSecret)

            do {
                
                try GTMOAuth2ViewControllerTouch.authorizeFromKeychainForName(kKeychainItemName, authentication: auth)
                    setAuthorizer(auth)
                    loginToBettrnet(){
                        (user) -> Void in
                        self.user = user
                        self.accountID = self.user.account.identifier.longLongValue
                        
                        
                        let path = NSBundle.mainBundle().pathForResource("deviceData", ofType: "plist")
                        
                        if let deviceData = NSDictionary(contentsOfFile: path!) {
                            
                            if let devID = deviceData.objectForKey("deviceID") as? NSNumber {
                                if devID != 0 {
                                    getDevice(self.accountID, deviceID: devID.longLongValue) {
                                        (device) -> Void in
                                        self.newDevice = device
                                        self.performSegueWithIdentifier("stats", sender: nil)
                                    }
                                }
                            }
                        }
                    }

            } catch {
                let authViewController:GTMOAuth2ViewControllerTouch = createAuthController()
                self.presentViewController(authViewController, animated: true, completion: nil)
            }
            
        }
            
    }

    // Creates the auth controller for authorizing access to Google Drive.
    func createAuthController() -> GTMOAuth2ViewControllerTouch {
        return GTMOAuth2ViewControllerTouch(scope: scope,
            clientID: kClientID,
            clientSecret: kClientSecret,
            keychainItemName: kKeychainItemName,
            delegate: self,
            finishedSelector: Selector("viewController:finishedWithAuth:error:"))
    }
    
    //     “func join(string s1: String, toString s2: String, withJoiner joiner: String)”
    
    // Handle completion of the authorization process, and updates the Drive service
    // with the new credentials.
    func viewController(viewController: GTMOAuth2ViewControllerTouch , finishedWithAuth authResult: GTMOAuth2Authentication , error:NSError? ) {
        if let err = error {
            self.showAlert("Authentication Error", message:err.localizedDescription)
        } else {
            print("Authentication success", authResult)
            viewController.dismissViewControllerAnimated(true, completion: nil)
            setAuthorizer(authResult)
            
            loginToBettrnet(){
                (user) -> Void in
                    self.user = user
                    self.accountID = self.user.account.identifier.longLongValue
            }

        }
    }
    
    @IBAction func createDeviceButtonTouched(sender: AnyObject) {
        
        if let deviceName = deviceNameInput.text, let wifiNetwork = wifiNetworkName.text {
            createDevice(self.accountID, deviceName: deviceName, wifiNetworkName: wifiNetwork) {
                (device) -> Void in
                    self.newDevice = device
                
                    let path = NSBundle.mainBundle().pathForResource("deviceData", ofType: "plist")
                
                    let data = NSMutableDictionary(contentsOfFile: path!)!
                    data.setObject((self.newDevice?.identifier)!, forKey: "deviceID")
                    data.writeToFile(path!, atomically: true)
                
                    print("If you are seeing this the device was created!!!! woo hoo")
                    print(self.newDevice!.JSONString())
                    
                    self.deviceNameInput.text = ""
                    self.createDeviceButton.hidden = true
                    self.deviceNameInput.hidden = true
                
                    self.performSegueWithIdentifier("stats", sender: nil)
            }
        }
        
        self.deviceNameInput.enabled = false
        self.createDeviceButton.enabled = false
        
    }
    
    let wifiNetworkNameString = ""
   
    @IBAction func wifiNetworkName(sender: AnyObject) {
        wifiNetworkName.text = wifiNetworkNameString
    }
    
    @IBAction func skipToStatsViewController(sender: UIButton) {
        listDevices(self.accountID) {
            (devices) -> Void in
                self.newDevice = devices.items().first as? GTLDevicesDeviceEntity
                self.performSegueWithIdentifier("stats", sender: nil)
        }

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "stats") {
            //get a reference to the destination view controller
            let destinationVC:StatsViewController = segue.destinationViewController as! StatsViewController
            
            //set properties on the destination view controller
            destinationVC.accountID = self.accountID
            destinationVC.deviceID = self.newDevice?.identifier.longLongValue

        }
    }
    


    
    func showAlert(title: String, message: String ) {
        let cancel = "OK"
        print("show Alert")
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButtonWithTitle(cancel)
        alert.show()
    }
}

