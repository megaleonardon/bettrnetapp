//
//  StatsViewController.swift
//  bettrnetApp
//
//  Created by David Leonard on 12/1/15.
//  Copyright © 2015 David Leonard. All rights reserved.
//

import Foundation

class StatsViewController: UIViewController{
    @IBOutlet weak var TotalTime: UILabel!
    
    var accountID:Int64!
    var deviceID:Int64!
    
    override func viewDidLoad() {

        
        super.viewDidLoad()
        
        
        generateTimeReport(accountID, deviceID: deviceID) {
            (report) -> Void in
                print("generating time report")
                print(report.timeReports[0].TotalTime)
                let totalMin = (report.timeReports[0].totalTime!.integerValue) / 60
                self.TotalTime.text = String(totalMin)
            
        }
    }
    
    
    
    
    
}