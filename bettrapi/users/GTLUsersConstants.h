/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLUsersConstants.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   users/v1
// Description:
//   This is an API

#import <Foundation/Foundation.h>

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLDefines.h"
#else
  #import "GTLDefines.h"
#endif

// Authorization scope
// View your email address
GTL_EXTERN NSString * const kGTLAuthScopeUsersUserinfoEmail;  // "https://www.googleapis.com/auth/userinfo.email"

// GTLUsers - BettrnetSubscriptionType
GTL_EXTERN NSString * const kGTLUsers_BettrnetSubscriptionType_BettrnetFamilyPlan;  // "bettrnet_family_plan"
GTL_EXTERN NSString * const kGTLUsers_BettrnetSubscriptionType_BettrnetSinglePlan5dollars;  // "bettrnet_single_plan_5dollars"
GTL_EXTERN NSString * const kGTLUsers_BettrnetSubscriptionType_BettrnetYearlyPlan;  // "bettrnet_yearly_plan"
GTL_EXTERN NSString * const kGTLUsers_BettrnetSubscriptionType_Other;  // "other"
