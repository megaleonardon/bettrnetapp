/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLDevicecontrolConstants.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   devicecontrol/v1
// Description:
//   This is an API

#import "GTLDevicecontrolConstants.h"

// Authorization scope
NSString * const kGTLAuthScopeDevicecontrolUserinfoEmail = @"https://www.googleapis.com/auth/userinfo.email";

// GTLQueryDevicecontrol - AccessSetting
NSString * const kGTLDevicecontrolAccessSettingGreen = @"Green";
NSString * const kGTLDevicecontrolAccessSettingRed   = @"Red";
