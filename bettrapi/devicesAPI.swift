
//
//  deviceApi.swift
//  bettrnetApp
//
//  Created by Troy Shields on 11/10/15.
//  Copyright © 2015 David Leonard. All rights reserved.
//

import Foundation




private var deviceSvcProvider = ServiceProvider<GTLServiceDevices>()

func createDevice(accountID: Int64, deviceName: String, wifiNetworkName: String, completionHandler: (device: GTLDevicesDeviceEntity) -> Void) {
    
    
    let createQuery = GTLQueryDevices.queryForCreateWithAccountId(accountID, name:deviceName);
    
    createQuery.setValue(wifiNetworkName, forKey: "assignedNetwork");
    createQuery.setValue(false, forKey: "isGlobalInstall");
    
    deviceSvcProvider.service.executeQuery(createQuery){
        (ticket, result, error) -> Void in
        if(error == nil){
            let newDevice = result as! GTLDevicesDeviceEntity
            print("If you are seeing this the device was created!!!! woo hoo");
            print(newDevice.JSONString())
            
            completionHandler(device: newDevice)
        }
    }
    
}

func listDevices(accountID: Int64, completionHandler: (devices: GTLDevicesDeviceEntityCollection) -> Void) {
    
    let query = GTLQueryDevices.queryForListDevicesWithAccountId(accountID);
    
    deviceSvcProvider.service.executeQuery(query){
        (ticket, result, error) -> Void in
        if(error == nil){
            let devices = result as! GTLDevicesDeviceEntityCollection
            print("If you are seeing this the devices were listed!!!! woo hoo");
            print(devices.JSONString())
            
            completionHandler(devices: devices)
        }
    }
    
}

func getDevice(accountID: Int64, deviceID: Int64, completionHandler: (devices: GTLDevicesDeviceEntity) -> Void) {
    
    let query = GTLQueryDevices.queryForRetrieveDeviceWithAccountId(accountID, deviceId: deviceID);
    
    deviceSvcProvider.service.executeQuery(query){
        (ticket, result, error) -> Void in
        if(error == nil){
            let device = result as! GTLDevicesDeviceEntity
            print("If you are seeing this the device was got!!!! woo hoo");
            print(device.JSONString())
            
            completionHandler(devices: device)
        }
    }
    
}