/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLQueryAccounts.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   accounts/v1
// Description:
//   This is an API
// Classes:
//   GTLQueryAccounts (7 custom class methods, 6 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLQuery.h"
#else
  #import "GTLQuery.h"
#endif

@interface GTLQueryAccounts : GTLQuery

//
// Parameters valid on all methods.
//

// Selector specifying which fields to include in a partial response.
@property (nonatomic, copy) NSString *fields;

//
// Method-specific parameters; see the comments below for more information.
//
@property (nonatomic, assign) long long accountId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL sendEmail;
@property (nonatomic, copy) NSString *subscriptionType;
@property (nonatomic, copy) NSString *tokenId;

#pragma mark - Service level methods
// These create a GTLQueryAccounts object.

// Method: accounts.activateAccount
//  Required:
//   subscriptionType:
//      kGTLAccountsSubscriptionTypeBettrnetFamilyPlan: "bettrnet_family_plan"
//      kGTLAccountsSubscriptionTypeBettrnetSinglePlan5dollars: "bettrnet_single_plan_5dollars"
//      kGTLAccountsSubscriptionTypeBettrnetYearlyPlan: "bettrnet_yearly_plan"
//      kGTLAccountsSubscriptionTypeOther: "other"
//  Optional:
//   tokenId: NSString
//  Authorization scope(s):
//   kGTLAuthScopeAccountsUserinfoEmail
// Fetches a GTLAccountsNinjaAccountEntity.
+ (instancetype)queryForActivateAccountWithAccountId:(long long)accountId
                                    subscriptionType:(NSString *)subscriptionType;

// Method: accounts.cancelAccount
//  Authorization scope(s):
//   kGTLAuthScopeAccountsUserinfoEmail
+ (instancetype)queryForCancelAccountWithAccountId:(long long)accountId;

// Method: accounts.deleteAccount
//  Authorization scope(s):
//   kGTLAuthScopeAccountsUserinfoEmail
+ (instancetype)queryForDeleteAccountWithAccountId:(long long)accountId;

// Method: accounts.list
//  Authorization scope(s):
//   kGTLAuthScopeAccountsUserinfoEmail
// Fetches a GTLAccountsNinjaAccountEntityCollection.
+ (instancetype)queryForList;

// Method: accounts.name
//  Authorization scope(s):
//   kGTLAuthScopeAccountsUserinfoEmail
// Fetches a GTLAccountsNinjaAccountEntity.
+ (instancetype)queryForNameWithAccountId:(long long)accountId
                                     name:(NSString *)name;

// Method: accounts.regenerateAccount
//  Authorization scope(s):
//   kGTLAuthScopeAccountsUserinfoEmail
+ (instancetype)queryForRegenerateAccountWithAccountId:(long long)accountId;

// Method: accounts.updateSendEmail
//  Optional:
//   sendEmail: Default true
//  Authorization scope(s):
//   kGTLAuthScopeAccountsUserinfoEmail
// Fetches a GTLAccountsNinjaAccountEntity.
+ (instancetype)queryForUpdateSendEmailWithAccountId:(long long)accountId;

@end
