/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLAccountsConstants.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   accounts/v1
// Description:
//   This is an API

#import <Foundation/Foundation.h>

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLDefines.h"
#else
  #import "GTLDefines.h"
#endif

// Authorization scope
// View your email address
GTL_EXTERN NSString * const kGTLAuthScopeAccountsUserinfoEmail;  // "https://www.googleapis.com/auth/userinfo.email"

// GTLAccounts - BettrnetSubscriptionType
GTL_EXTERN NSString * const kGTLAccounts_BettrnetSubscriptionType_BettrnetFamilyPlan;  // "bettrnet_family_plan"
GTL_EXTERN NSString * const kGTLAccounts_BettrnetSubscriptionType_BettrnetSinglePlan5dollars;  // "bettrnet_single_plan_5dollars"
GTL_EXTERN NSString * const kGTLAccounts_BettrnetSubscriptionType_BettrnetYearlyPlan;  // "bettrnet_yearly_plan"
GTL_EXTERN NSString * const kGTLAccounts_BettrnetSubscriptionType_Other;  // "other"

// GTLQueryAccounts - SubscriptionType
GTL_EXTERN NSString * const kGTLAccountsSubscriptionTypeBettrnetFamilyPlan;  // "bettrnet_family_plan"
GTL_EXTERN NSString * const kGTLAccountsSubscriptionTypeBettrnetSinglePlan5dollars;  // "bettrnet_single_plan_5dollars"
GTL_EXTERN NSString * const kGTLAccountsSubscriptionTypeBettrnetYearlyPlan;  // "bettrnet_yearly_plan"
GTL_EXTERN NSString * const kGTLAccountsSubscriptionTypeOther;  // "other"
