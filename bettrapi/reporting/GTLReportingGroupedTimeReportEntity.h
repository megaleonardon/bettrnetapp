/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLReportingGroupedTimeReportEntity.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   reporting/v2
// Description:
//   This is an API
// Classes:
//   GTLReportingGroupedTimeReportEntity (0 custom class methods, 1 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLObject.h"
#else
  #import "GTLObject.h"
#endif

@class GTLReportingTimeReportEntityV2;

// ----------------------------------------------------------------------------
//
//   GTLReportingGroupedTimeReportEntity
//

@interface GTLReportingGroupedTimeReportEntity : GTLObject
@property (nonatomic, retain) NSArray *timeReports;  // of GTLReportingTimeReportEntityV2
@end
