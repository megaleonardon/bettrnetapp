/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLRulesRuleEntity.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   rules/v1
// Description:
//   This is an API
// Classes:
//   GTLRulesRuleEntity (0 custom class methods, 7 custom properties)

#import "GTLRulesRuleEntity.h"

#import "GTLRulesChannelEntity.h"
#import "GTLRulesScheduleEntity.h"

// ----------------------------------------------------------------------------
//
//   GTLRulesRuleEntity
//

@implementation GTLRulesRuleEntity
@dynamic accountId, channels, deviceIds, enabled, identifier, ruleName,
         schedules;

+ (NSDictionary *)propertyToJSONKeyMap {
  NSDictionary *map = @{
    @"identifier" : @"id"
  };
  return map;
}

+ (NSDictionary *)arrayPropertyToClassMap {
  NSDictionary *map = @{
    @"channels" : [GTLRulesChannelEntity class],
    @"deviceIds" : [NSNumber class],
    @"schedules" : [GTLRulesScheduleEntity class]
  };
  return map;
}

@end
