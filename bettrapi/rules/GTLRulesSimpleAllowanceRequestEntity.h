/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLRulesSimpleAllowanceRequestEntity.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   rules/v1
// Description:
//   This is an API
// Classes:
//   GTLRulesSimpleAllowanceRequestEntity (0 custom class methods, 4 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLObject.h"
#else
  #import "GTLObject.h"
#endif

// ----------------------------------------------------------------------------
//
//   GTLRulesSimpleAllowanceRequestEntity
//

@interface GTLRulesSimpleAllowanceRequestEntity : GTLObject
@property (nonatomic, retain) NSNumber *allowanceTimeSeconds;  // longLongValue
@property (nonatomic, retain) NSArray *deviceIds;  // of NSNumber (longLongValue)
@property (nonatomic, retain) NSNumber *periodTimeSeconds;  // longLongValue
@property (nonatomic, copy) NSString *ruleName;
@end
