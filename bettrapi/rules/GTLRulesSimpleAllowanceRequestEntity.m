/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLRulesSimpleAllowanceRequestEntity.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   rules/v1
// Description:
//   This is an API
// Classes:
//   GTLRulesSimpleAllowanceRequestEntity (0 custom class methods, 4 custom properties)

#import "GTLRulesSimpleAllowanceRequestEntity.h"

// ----------------------------------------------------------------------------
//
//   GTLRulesSimpleAllowanceRequestEntity
//

@implementation GTLRulesSimpleAllowanceRequestEntity
@dynamic allowanceTimeSeconds, deviceIds, periodTimeSeconds, ruleName;

+ (NSDictionary *)arrayPropertyToClassMap {
  NSDictionary *map = @{
    @"deviceIds" : [NSNumber class]
  };
  return map;
}

@end
