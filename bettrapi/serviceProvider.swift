//
//  authorize.swift
//  bettrnetApp
//
//  Created by Troy Shields on 11/11/15.
//  Copyright © 2015 David Leonard. All rights reserved.
//

import Foundation

//Should this auth stuff be moved to a different file? How should we clean it up to make it more straight-forward?
private struct GoogleAuthorizer {
    let authorizer: GTMFetcherAuthorizationProtocol
}

private var googleAuthorizer: GoogleAuthorizer? = nil;

func setAuthorizer(auth: GTMFetcherAuthorizationProtocol) {
    if let _ = googleAuthorizer {
        //Do nothing?
    }
    else {
        googleAuthorizer = GoogleAuthorizer(authorizer: auth)
    }
}

func isAuthed() -> Bool {
    if let auth = googleAuthorizer {
        return auth.authorizer.canAuthorize!
    }
    else {
        return false
    }
}



struct ServiceProvider<T: GTLService> {
    private var instance: T?
    
    var service: T {
        mutating get {
            if let svc = self.instance {
                return svc
            }
            else { //TODO What about if there is no authorizer yet? How do we handle that?
                let svcAuth = googleAuthorizer!
                self.instance = T()
                self.instance?.authorizer = svcAuth.authorizer
                return self.instance!
            }
        }
    }
}