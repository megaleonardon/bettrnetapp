//
//  userApi.swift
//  bettrnetApp
//
//  Created by Troy Shields on 11/10/15.
//  Copyright © 2015 David Leonard. All rights reserved.
//

import Foundation


private var userSvcProvider = ServiceProvider<GTLServiceUsers>()

func loginToBettrnet(completionHandler: (user: GTLUsersNinjaOwnerEntity) -> Void) {
    
    userSvcProvider.service.executeQuery(GTLQueryUsers.queryForLogin()) {
        (ticket, result, error) -> Void in
        let user = result as! GTLUsersNinjaOwnerEntity
        print(user.JSONString())
        completionHandler(user: user)
    }
    
}
