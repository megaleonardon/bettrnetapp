/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2015 Google Inc.
 */

//
//  GTLQueryChannels.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   channels/v1
// Description:
//   This is an API
// Classes:
//   GTLQueryChannels (6 custom class methods, 4 custom properties)

#import "GTLQueryChannels.h"

#import "GTLChannelsChannelEntity.h"
#import "GTLChannelsChannelEntityCollection.h"
#import "GTLChannelsChannelIdentifierEntity.h"

@implementation GTLQueryChannels

@dynamic channelId, channelIdentifierId, fields, identifierId;

#pragma mark - Service level methods
// These create a GTLQueryChannels object.

+ (instancetype)queryForAddChannelIdentifierWithObject:(GTLChannelsChannelIdentifierEntity *)object
                                             channelId:(long long)channelId {
  if (object == nil) {
    GTL_DEBUG_ASSERT(object != nil, @"%@ got a nil object", NSStringFromSelector(_cmd));
    return nil;
  }
  NSString *methodName = @"channels.addChannelIdentifier";
  GTLQueryChannels *query = [self queryWithMethodName:methodName];
  query.bodyObject = object;
  query.channelId = channelId;
  query.expectedObjectClass = [GTLChannelsChannelEntity class];
  return query;
}

+ (instancetype)queryForCreateChannelWithObject:(GTLChannelsChannelEntity *)object {
  if (object == nil) {
    GTL_DEBUG_ASSERT(object != nil, @"%@ got a nil object", NSStringFromSelector(_cmd));
    return nil;
  }
  NSString *methodName = @"channels.createChannel";
  GTLQueryChannels *query = [self queryWithMethodName:methodName];
  query.bodyObject = object;
  query.expectedObjectClass = [GTLChannelsChannelEntity class];
  return query;
}

+ (instancetype)queryForDeleteChannelWithChannelId:(long long)channelId {
  NSString *methodName = @"channels.deleteChannel";
  GTLQueryChannels *query = [self queryWithMethodName:methodName];
  query.channelId = channelId;
  return query;
}

+ (instancetype)queryForDeleteChannelIdentifierWithChannelId:(long long)channelId
                                                identifierId:(long long)identifierId {
  NSString *methodName = @"channels.deleteChannelIdentifier";
  GTLQueryChannels *query = [self queryWithMethodName:methodName];
  query.channelId = channelId;
  query.identifierId = identifierId;
  return query;
}

+ (instancetype)queryForListChannels {
  NSString *methodName = @"channels.listChannels";
  GTLQueryChannels *query = [self queryWithMethodName:methodName];
  query.expectedObjectClass = [GTLChannelsChannelEntityCollection class];
  return query;
}

+ (instancetype)queryForUpdateChannelIdentifierWithObject:(GTLChannelsChannelIdentifierEntity *)object
                                                channelId:(long long)channelId
                                      channelIdentifierId:(long long)channelIdentifierId {
  if (object == nil) {
    GTL_DEBUG_ASSERT(object != nil, @"%@ got a nil object", NSStringFromSelector(_cmd));
    return nil;
  }
  NSString *methodName = @"channels.updateChannelIdentifier";
  GTLQueryChannels *query = [self queryWithMethodName:methodName];
  query.bodyObject = object;
  query.channelId = channelId;
  query.channelIdentifierId = channelIdentifierId;
  query.expectedObjectClass = [GTLChannelsChannelEntity class];
  return query;
}

@end
