//
//  reportingAPI.swift
//  bettrnetApp
//
//  Created by David Leonard on 12/1/15.
//  Copyright © 2015 David Leonard. All rights reserved.
//

import Foundation


private var reportingProvider = ServiceProvider<GTLServiceReporting>()

func generateTimeReport(accountID: Int64, deviceID: Int64, completionHandler: (report: GTLReportingGroupedTimeReportEntity) -> Void) {
    
    
    let query = GTLQueryReporting.queryForGenerateTimeReportWithAccountId(accountID, deviceId: deviceID)
    reportingProvider.service.executeQuery(query) {
        (ticket, result, error) -> Void in
        if(error == nil){
            let newReport = result as! GTLReportingGroupedTimeReportEntity
            print("REPORT - ITS WORKINGS!!")
            print(newReport.JSONString())
            
            completionHandler(report: newReport)
        }
    }
    
}